extends Node

onready var status = get_node("/root/SceneManager/UI/LoginScreen/NinePatchRect/Login/Status")
var network = NetworkedMultiplayerENet.new()
#var ip = "127.0.0.1"
var ip = "23.88.120.66"
var port = 1909
var token

func _ready():
	pass
	#ConnectToServer()


### Connection ###

func ConnectToServer():
	network.create_client(ip,port)
	get_tree().set_network_peer(network)
	
	network.connect("connection_failed", self, "_OnConnectionFailed")
	network.connect("connection_succeeded", self, "_OnConnectionSucceeded")
	
func _OnConnectionFailed():
	print("Failed to connect to the game server")
	status.set_text("Failed to connect to the game server")
	get_node("/root/SceneManager/UI/LoginScreen").status_label.set_text("Login timed out")
	
func _OnConnectionSucceeded():
	print("Succesfully connected to the game server")
	status.set_text("Succesfully connected to the game server")
	
remote func FetchToken():
	rpc_id(1, "ReturnToken", token)

remote func ReturnTokenVerificationResults(result):
	if result:
		get_node("/root/SceneManager/UI/LoginScreen").queue_free()
		#FetchPlayerStats()
		Utils.get_scene_manager().transition_to_scene("res://World/PalletTown/PlayerHomeFloor1.tscn",Vector2(144,80),Vector2(0,1))
		print("Token valid")
		status.set_text("Token valid")
	else:
		print("Login Failed please try again")
		status.set_text("Login Failed please try again")
		get_node("/root/SceneManager/UI/LoginScreen").login_button.disabled = false
		get_node("/root/SceneManager/UI/LoginScreen").create_account_button.disabled = false





remote func RecieveWorldState(world_state):
	Utils.get_current_scene().UpdateWorldState(world_state)

remote func SpawnNewPlayer(player_id, spawn_position):
	get_node("/root/SceneManager/CurrentScene").SpawnNewPlayer(player_id, spawn_position)

remote func DespawnPlayer(player_id):
	get_node("/root/SceneManager/CurrentScene").DespawnPlayer(player_id)

func SendPlayerData(player_data):
	rpc_unreliable_id(1,"RecievePlayerData", player_data)



### Test ###

func FetchPlayerStats():
	rpc_id(1,"FetchPlayerStats")
	
remote func ReturnPlayerStats(stats):
	Utils.get_current_scene_level().get_node("YSort/Player").LoadPlayerStats(stats)
