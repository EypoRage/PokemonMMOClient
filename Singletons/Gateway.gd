extends Node

onready var status = get_node("/root/SceneManager/UI/LoginScreen/NinePatchRect/Login/Status")
onready var create_status = get_node("/root/SceneManager/UI/LoginScreen/NinePatchRect/CreateAccount/Status")


var network = NetworkedMultiplayerENet.new()
var gateway_api = MultiplayerAPI.new()
#var ip = "127.0.0.1"
var ip = "23.88.120.66"
var port = 1910
var cert = load("res://Util/Certificate/X509_Certificate.crt")

var username
var password
var new_account = false


func _ready():
	pass
	
	
func _process(delta):
	if get_custom_multiplayer() == null:
		return
	if not custom_multiplayer.has_network_peer():
		return
	custom_multiplayer.poll()
	
	
func ConnectToServer(_username, _password, _new_account):
	network = NetworkedMultiplayerENet.new()
	gateway_api = MultiplayerAPI.new()
	network.set_dtls_enabled(true)
	network.set_dtls_verify_enabled(false)
	network.set_dtls_certificate(cert)
	username = _username.to_lower()
	password = _password
	new_account = _new_account
	var err = network.create_client(ip,port)
	if err:
		return false
	set_custom_multiplayer(gateway_api)
	custom_multiplayer.set_root_node(self)
	custom_multiplayer.set_network_peer(network)
	
	
	network.connect("connection_failed", self, "_OnConnectionFailed")
	network.connect("connection_succeeded", self, "_OnConnectionSucceeded")
	
	return true

	
func _OnConnectionFailed():
	print("Failed to connect to login server")
	print("Pop-up server offline or something")
	status.set_text("Failed to connect to login server")
	get_node("/root/SceneManager/UI/LoginScreen").login_button.disabled = false
	get_node("/root/SceneManager/UI/LoginScreen").create_account_button.disabled = false
	get_node("/root/SceneManager/UI/LoginScreen").confirm_button.disabled = false
	get_node("/root/SceneManager/UI/LoginScreen").back_button.disabled = false
	
func _OnConnectionSucceeded():
	print("Sucesfully connected to login server")
	status.set_text("Sucesfully connected to login server")
	if new_account == true:
		RequestCreateAccount()
	else:
		RequestLogin()
	


func RequestLogin():
	print("request login via gateway")
	status.set_text("request login via gateway")
	
	rpc_id(1, "LoginRequest", username, password)
	username = ""
	password = ""


remote func ReturnLoginRequest(results,token):
	print("results recieved")
	if results == true:
		GameServer.token = token
		GameServer.ConnectToServer()
		status.set_text("results recieved")
	else:
		print("please provide a correct username and password")
		status.set_text("please provide a correct username and password")
		get_node("/root/SceneManager/UI/LoginScreen").login_button.disabled = false
		get_node("/root/SceneManager/UI/LoginScreen").create_account_button.disabled = false
	network.disconnect("connection_failed",self,"_OnConnectionFailed")
	network.disconnect("connection_succeeded", self, "_OnConnectionSucceeded")

func RequestCreateAccount():
	print("Requesting new account")
	rpc_id(1, "CreateAccountRequest", username, password)
	username = ""
	password = ""

remote func ReturnCreateAccountRequest(results, message):
	print("create account results recieved")
	create_status.set_text("create account results recieved")
	if results == true:
		print("Account created, please proceed to log in")
		create_status.set_text("Account created, please proceed to log in")
		status.set_text("Account created, please proceed to log in")
		get_node("/root/SceneManager/UI/LoginScreen")._on_Back_pressed()
	else:
		if message == 1:
			print("Could not create account, please try again")
			create_status.set_text("Could not create account, please try again")
		elif message == 2:
			print("The username already exist, plase use a different username.")
			create_status.set_text("The username already exist, plase use a different username.")
		get_node("/root/SceneManager/UI/LoginScreen").confirm_button.disabled = false
		get_node("/root/SceneManager/UI/LoginScreen").back_button.disabled = false
	network.disconnect("connection_failed",self,"_OnConnectionFailed")
	network.disconnect("connection_succeeded", self, "_OnConnectionSucceeded")
	
