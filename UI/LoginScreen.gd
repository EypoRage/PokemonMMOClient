extends Control

# ui states
onready var login_screen = get_node("NinePatchRect/Login")
onready var create_account_screen = get_node("NinePatchRect/CreateAccount")

#login nodes
onready var username_input = get_node("NinePatchRect/Login/Username")
onready var password_input = get_node("NinePatchRect/Login/Password")
onready var login_button = get_node("NinePatchRect/Login/LoginButton")
onready var status_label = get_node("NinePatchRect/Login/Status")
onready var create_account_button = get_node("NinePatchRect/Login/CreateAccountButton")

#create account nodes
onready var create_username_input = get_node("NinePatchRect/CreateAccount/Username")
onready var create_password_input = get_node("NinePatchRect/CreateAccount/Password")
onready var create_password_repeat_input = get_node("NinePatchRect/CreateAccount/PasswordRepeat")
onready var confirm_button = get_node("NinePatchRect/CreateAccount/HBoxContainer/Confirm")
onready var back_button = get_node("NinePatchRect/CreateAccount/HBoxContainer/Back")
onready var create_status_label = get_node("NinePatchRect/CreateAccount/Status")


func _on_LoginButton_pressed():
	if username_input.text == "" or password_input.text == "":
		#popup and stop
		status_label.set_text("Please provide valid userid and password")
		print("Please provide valid userid and password")
	else:
		login_button.disabled = true
		create_account_button.disabled = true
		var username = username_input.get_text()
		var password = password_input.get_text()
		Utils.username = username
		status_label.set_text("attempting to login")
		print("attempting to login")
		var err = Gateway.ConnectToServer(username, password, false)
		if err:
			status_label.set_text("Connecting to Gateway ...")
			print("connecting to gateway ...")
			login_button.disabled = false
			create_account_button.disabled = false


func _on_CreateAccountButton_pressed():
	login_screen.hide()
	create_account_screen.show()


func _on_Back_pressed():
	create_account_screen.hide()
	login_screen.show()

func _on_Confirm_pressed():
	if create_password_input.get_text() == "":
		print("Please provide a valid username")
		create_status_label.set_text("Please provide a valid username")
	elif create_password_input.get_text() == "":
		print("Please provide a valid password")
		create_status_label.set_text("Please provide a valid password")
	elif create_password_repeat_input.get_text() == "":
		print("Please repeat your password")
		create_status_label.set_text("Please repeat your password")
	elif create_password_input.get_text() != create_password_repeat_input.get_text():
		print("Passwords do not match")
		create_status_label.set_text("Passwords do not match")
	elif create_password_input.get_text().length() <= 6:
		print("Password must contain at least 7 characters")
		create_status_label.set_text("Password must contain at least 7 characters")
	else:
		confirm_button.disabled = true
		back_button.disabled = true
		var username = create_username_input.get_text()
		var password = create_password_input.get_text()
		var err = Gateway.ConnectToServer(username, password, true)
		if err:
			create_status_label.set_text("Connecting to Gateway ...")
			print("Connecting to gateway ...")
			confirm_button.disabled = false
			back_button.disabled = false
		


